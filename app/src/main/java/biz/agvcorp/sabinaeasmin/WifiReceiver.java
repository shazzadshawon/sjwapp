package biz.agvcorp.sabinaeasmin;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import static biz.agvcorp.sabinaeasmin.MainActivity.websiteUrl;
import static biz.agvcorp.sabinaeasmin.MainActivity.websiteUrlHeader;

/**
 * Created by Shazzad on 1/30/2018.
 */

public class WifiReceiver extends BroadcastReceiver {

    private Context targetContext;
    private Intent targetIntent;
    private WebView targetView;
    private WebViewClient webViewClientClass;

    WifiReceiver(){}

    @Override
    public void onReceive(Context context, Intent intent) {
        ConnectivityManager conMan = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMan.getActiveNetworkInfo();

        if ( netInfo != null && netInfo.getType() == ConnectivityManager.TYPE_WIFI ){
            targetView.loadUrl(websiteUrlHeader+websiteUrl);
            targetView.setWebViewClient(webViewClientClass);
        }
    }

    public WifiReceiver(Context context, Intent intent, WebView webView, WebViewClient targetWebViewClient){
        targetContext = context;
        targetIntent = intent;
        targetView = webView;
        webViewClientClass = targetWebViewClient;
    }
}
